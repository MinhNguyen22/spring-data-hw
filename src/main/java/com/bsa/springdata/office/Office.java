package com.bsa.springdata.office;

import com.bsa.springdata.user.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

// TODO: Map table offices to this entity
@Entity
@Table(name = "offices")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Office {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    private UUID id;
    @Column
    private String city;
    @Column
    private String address;
    @OneToMany(mappedBy = "office", cascade = CascadeType.ALL)
    private List<User> users;
}
