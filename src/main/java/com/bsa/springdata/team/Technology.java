package com.bsa.springdata.team;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

// TODO: Map table technologies to this entity
@Entity
@Data
@Table(name = "technologies")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Technology {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    private UUID id;
    @Column
    private String name;
    @Column
    private String description;
    @Column
    private String link;
    @OneToMany(mappedBy = "technology")
    private List<Team> teams;
}