package com.bsa.springdata.team;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.UUID;
@Repository
public interface TeamRepository extends JpaRepository<Team, UUID> {
    Optional<Team> findByName(String name);


    int countByTechnologyName(String name);

    @Modifying
    @Transactional
    @Query(nativeQuery = true, value = "UPDATE teams " +
            "SET technology_id = (SELECT id FROM technologies WHERE NAME = :new_tech limit 1) " +
            "FROM ( " +
            "SELECT t.id team_id " +
            "FROM technologies tech " +
            "INNER JOIN teams t ON tech.id = t.technology_id " +
            "INNER JOIN users u ON t.id = u.team_id " +
            "WHERE tech.name = :technology " +
            "GROUP BY t.id " +
            "HAVING COUNT(u.id) < :devsNumber " +
            ") tech " +
            "WHERE tech.team_id = teams.id;")
    void updateName(@Param("devsNumber") int devsNumber,
                    @Param("technology") String oldTechnologyName,
                    @Param("new_tech") String newTechnologyName);

    @Modifying
    @Transactional
    @Query(nativeQuery = true, value = "UPDATE teams AS t " +
            "SET NAME = t.name || '_' || p.name || '_' || tech.name " +
            "FROM technologies AS tech, projects AS p " +
            "WHERE t.project_id = p.id AND tech.id = t.technology_id AND t.name = :hipsters")
    void normalizeName(@Param("hipsters") String hipsters);

}
