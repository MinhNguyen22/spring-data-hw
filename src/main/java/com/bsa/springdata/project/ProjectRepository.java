package com.bsa.springdata.project;

import com.bsa.springdata.project.dto.ProjectSummaryDto;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;

public interface ProjectRepository extends JpaRepository<Project, UUID> {

    @Query(nativeQuery = true, value = "SELECT p.*, count(u.id) AS countUser FROM projects p\n" +
            "INNER JOIN teams t ON t.project_id =  p.id\n" +
            "INNER JOIN technologies tech ON t.technology_id = tech.id\n" +
            "INNER JOIN users u ON u.team_id = t.id " +
            "WHERE tech.name = :technology " +
            "GROUP BY p.id " +
            "ORDER BY countUser DESC")
    List<Project> findTop5ByTechnology(@Param("technology") String technology, Pageable pageable);

    @Query(value = "SELECT p.*, COUNT(t.id) AS countTeam, count(u.id) AS countUser " +
            "FROM projects p " +
            "INNER JOIN teams t ON p.id = t.project_id " +
            "INNER JOIN users u ON t.id = u.team_id " +
            "GROUP BY p.id " +
            "ORDER BY countTeam DESC, countUser DESC, p.name DESC limit 1", nativeQuery = true)
    List<Project> findTheBiggest();

    @Query(nativeQuery = true, value = "SELECT p.name, " +
            "COUNT(DISTINCT t2) AS teamsnumber, " +
            "COUNT(u2) AS devsnumber, " +
            "array_to_string(array_agg(DISTINCT t3.name ORDER BY t3.name DESC), ',') techs " +
            "FROM projects AS p " +
            "INNER JOIN teams t2 ON p.id = t2.project_id " +
            "INNER JOIN technologies t3 ON t2.technology_id = t3.id " +
            "INNER JOIN users u2 ON t2.id = u2.team_id " +
            "GROUP BY p.id " +
            "ORDER BY p.name ")
    List<ProjectSummaryDto> getSummary();

    @Query(value = "SELECT count(t.proj_id) FROM (SELECT proj.id AS proj_id FROM projects proj " +
            "INNER JOIN teams t ON proj.id = t.project_id " +
            "INNER JOIN users u ON t.id = u.team_id " +
            "INNER JOIN user2role u2r ON u.id = u2r.user_id " +
            "INNER JOIN roles r ON u2r.role_id = r.id " +
            "WHERE r.name = :role " +
            "GROUP BY proj.id " +
            ") t", nativeQuery = true)
    int countByUsersRoles(@Param("role") String role);
}